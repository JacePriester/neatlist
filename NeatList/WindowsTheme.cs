﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WinapiWindowingSystem;

namespace WindowsTheme
{
    public class ThemeColorData
    {
        public Color BaseColor;
        public Color Lighter;
        public Color Darker;
        public Color ExtraLight;
        public Color ExtraDark;

        public ThemeColorData(Color BaseColor)
        {
            this.BaseColor = BaseColor;
            Lighter = ThemeInfo.BlendColor(BaseColor, 0.3, Color.White);
            Darker = ThemeInfo.BlendColor(BaseColor, 0.3, Color.Black);

            ExtraLight = ThemeInfo.BlendColor(BaseColor, 0.6, Color.White);
            ExtraDark = ThemeInfo.BlendColor(BaseColor, 0.6, Color.Black);
        }

    }

    public static class ThemeInfo
    {
        public static Color BlendColor(Color basecolor, double factor, Color blend)
        {
            int a0 = basecolor.A, r0 = basecolor.R, g0 = basecolor.G, b0 = basecolor.B;
            int a1 = blend.A, r1 = blend.R, g1 = blend.G, b1 = blend.B;

            return Color.FromArgb((byte)(a0 + factor * (a1 - a0)),
                (byte)(r0 + factor * (r1 - r0)),
                (byte)(g0 + factor * (g1 - g0)),
                (byte)(b0 + factor * (b1 - b0)));
        }

        static ThemeColorData last_tcd = null;
        
        public static ThemeColorData GetThemeColor()
        {
            //capture the task bar instead. this should work everywhere.
            TaskbarLocator.Taskbar.Refresh();
            IntPtr hwnd_taskbar = TaskbarLocator.Taskbar.Handle;

            if (hwnd_taskbar != IntPtr.Zero)
            {
                List<IntPtr> children = WindowTools.GetChildHandles(hwnd_taskbar);
                IntPtr rebar = WindowTools.FindByClass(children, "rebarwindow32");

                if (rebar != IntPtr.Zero)
                {
                    WindowTools.RECT rebar_rect;
                    WindowTools.GetWindowRect(rebar, out rebar_rect);

                    Rectangle capture_rect = new Rectangle(rebar_rect.Left - 4, 4, 4, rebar_rect.Bottom - rebar_rect.Top - 4 - 2);

                    Point screen_topleft = new Point(capture_rect.Left, capture_rect.Top);
                    WindowTools.ClientToScreen(hwnd_taskbar, ref screen_topleft);

                    capture_rect.X = screen_topleft.X;
                    capture_rect.Y = screen_topleft.Y;

                    Bitmap captured_bitmap = new Bitmap(capture_rect.Width, capture_rect.Height, PixelFormat.Format32bppArgb);
                    using (Graphics graphics = Graphics.FromImage(captured_bitmap))
                    {
                        graphics.CopyFromScreen(capture_rect.Left, capture_rect.Top, 0, 0, new Size(captured_bitmap.Width, captured_bitmap.Height), CopyPixelOperation.SourceCopy);
                    }

                    ThemeColorData tcd = new ThemeColorData(CalcBitmapAverageColor(captured_bitmap));
                    last_tcd = tcd;

                    return tcd;
                }
            }

            //if we can't get the theme color just make up a generic gray color. looks ok.
            if (last_tcd == null)
                last_tcd = new ThemeColorData(Color.FromArgb(255, 78, 78, 78));

            return last_tcd;
        }

        static Color CalcBitmapAverageColor(Bitmap bm)
        {
            BitmapData srcData = bm.LockBits(new Rectangle(0, 0, bm.Width, bm.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            int stride = srcData.Stride;

            IntPtr Scan0 = srcData.Scan0;

            long[] totals = new long[] { 0, 0, 0 };

            int width = bm.Width;
            int height = bm.Height;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        for (int color = 0; color < 3; color++)
                        {
                            int idx = (y * stride) + x * 4 + color;

                            totals[color] += p[idx];
                        }
                    }
                }
            }

            int npixels = width * height;

            int avgB = (int)(totals[0] / npixels);
            int avgG = (int)(totals[1] / npixels);
            int avgR = (int)(totals[2] / npixels);

            return Color.FromArgb(255, avgR, avgG, avgB);
        }
    }
}
