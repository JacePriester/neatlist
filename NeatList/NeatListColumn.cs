﻿using System.Drawing;
using System.Windows.Forms;

namespace NeatList
{
    public class NeatListColumn
    {
        public NeatListView ParentListView;

        public string Text = "";
        public object TheObject; //if you want to attach a custom object, maybe custom draw, go for it

        //these used by AttributeListView/AttributeListItem for drawing text/icons
        public string AttributeName = "";
        public bool AttributeIsProperty = false;
        public bool IsIconColumn = false;

        public double Size; //either % or px depending on how set in the listview
        public bool SizeFillsRemainingSpace = false; //can only be set for ONE column, only valid if in pixel mode
        public bool FixedPixelSizeWhenResized = false; //valid for multiple columns
        
        public Rectangle Bounds; //bounds of column header

        public Rectangle TextBounds_Local;

        //an ID set by the caller; can be used for identifying columns in sorters or whatever
        public int ID = 0;
        
        public virtual void CalculateHeaderBounds(NeatListColumn previous, int assigned_width)
        {
            if (Size <= 0.0 || ParentListView.ClientRectangle.Width <= 0)
                return;

            int left = (previous == null) ? 0 : previous.Bounds.Right;

            int VMargin = 5;
            int HMargin = 4;

            Bounds = new Rectangle(left, 0, assigned_width, ParentListView.Internals.TextLineHeight + VMargin * 2);

            TextBounds_Local = new Rectangle(HMargin, (Bounds.Height - ParentListView.Internals.TextLineHeight) / 2, Bounds.Width - HMargin, ParentListView.Internals.TextLineHeight);
        }

        public NeatListColumn()
        {
        }
        
        public NeatListColumn(NeatListView ParentListView, int ID, string Text, int Size, bool SizeFillsRemainingSpace)
        {
            this.ParentListView = ParentListView;
            this.ID = ID;
            this.Text = Text;
            this.Size = Size;
            this.SizeFillsRemainingSpace = SizeFillsRemainingSpace;
        }
        
        public virtual Rectangle GetHitBounds()
        {
            //just return bounds, headers dont scroll
            return Bounds;
        }

        public void Draw(Graphics graphics)
        {
            graphics.FillRectangle(ParentListView.Internals.Brushes["column_header_background"], Bounds);
            graphics.DrawRectangle(ParentListView.Internals.Pens["column_header_border"], Bounds);

            //var fmt = new StringFormat();
            //fmt.Alignment = StringAlignment.Near;
            //fmt.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.NoClip | StringFormatFlags.LineLimit;
            //fmt.LineAlignment = StringAlignment.Near;
            //fmt.Trimming = StringTrimming.None;

            //RectangleF recf = new RectangleF(TextBounds_Local.Left, TextBounds_Local.Top, TextBounds_Local.Width, TextBounds_Local.Height);
            //recf.Offset(Bounds.Location);

            Point textloc = TextBounds_Local.Location;
            textloc.Offset(Bounds.Location);

            //graphics.DrawString(Text, ParentListView.Font, ParentListView.Internals.Brushes["text"], recf, fmt);

            TextRenderer.DrawText(graphics, Text, ParentListView.Font, textloc, ParentListView.ForeColor);
        }
    }
}
