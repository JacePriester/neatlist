﻿using System.Collections.Generic;
using System.Drawing;

namespace NeatList
{
    public enum NeatItemLayout
    {
        Rows,
        Columns,
        GridLeftToRight,
        GridTopToBottom
    }

    public enum NeatListFormat
    {
        Simple,
        MultipleColumns,
        Tree
    }

    public class NeatListItemComparer<ObjectType> : IComparer<NeatListItem>
    {
        public IComparer<ObjectType> ObjectComparer;

        public NeatListItemComparer(IComparer<ObjectType> ObjectComparer)
        {
            this.ObjectComparer = ObjectComparer;
        }

        public int Compare(NeatListItem a, NeatListItem b)
        {
            return ObjectComparer.Compare((ObjectType)a.TheObject, (ObjectType)b.TheObject);
        }
    }

    public partial class NeatListView
    {
        public class _internal_Properties
        {
            //public stuff
            public bool ShowHighlightWhenNotFocused = false;
            public bool ShowSelectionWhenNotFocused = true;
            public bool ShowSelectionAlternateStyleWhenNotFocused = true;
            public bool KeepHighlightWhenMouseLeavesValidObject = false;

            public Color AlternateRowBackColor = Color.FromArgb(255, 240, 240, 255);
            public Color HighlightColor = Color.Green;
            public float HighlightPenThickness = 2.0f;
            public Color NoIconColor = Color.FromArgb(255, 100, 100, 100);
            public Color SelectionColor = Color.LightSkyBlue;
            public Color SelectionColorWhileUnfocused = Color.LightGray;

            public Font EmptyListFont = null; //generated at run-time unless set by outside code
            public string EmptyListMessage = "Nothing to Show";
            public Color EmptyListBackColor = Color.LightGoldenrodYellow;
            public Color EmptyListBorderColor = Color.Black;
            public float EmptyListBorderThickness = 2.0f;
            public int EmptyListMessageMargin = 15;
            public int EmptyListBorderRadius = 7;

            public bool SupportsHighlight = true;
            public bool SupportsSelection = true;
            public bool SupportsMultiSelect = false;

            //for non-linear lists, may want to use shift just like ctrl instead.
            public bool TreatShiftLikeControl = false;

            public bool SupportsItemActivation = true;
            public bool ItemActivationRequiresDoubleClick = false; //must be set before the control is shown for the first time

            public bool CursorHandOnClickableItem = true;

            public bool DrawSelectionBehindObject = true; //if true, selection is drawn first, then object. if false, object then selection.

            //these are really just suggestions since the listitems calculate their own bounds
            public int FixedRowHeight = -1;
            public float SeperatorHeightScale = 0.5f;
            public int FixedColumnWidth = -1;
            public float SeperatorWidthScale = 0.5f;

            public NeatItemLayout ItemLayout = NeatItemLayout.Rows; //use the predefined layouts from NeatItemLayout or specifiy your own and handle it in the item's Recalculate function.

            public int LeftMargin = 0;
            public int TopMargin = 0;
            public int RightMargin = 0;
            public int BottomMargin = 0;

            public NeatListFormat ListFormat = NeatListFormat.Simple; //or multicolumn, or tree

            public bool KeepAtLeastOneItemSelected = false; //currently only applies to the user-selection process.

            public bool FocusOnMouseDown = true;
            public bool FocusOnMouseEnter = false;

            public IComparer<NeatListItem> SortComparer = null;

            public bool EnableVerticalScroll = true;
            public bool EnableHorizontalScroll = false;
            public bool ExpandContentWhenScrollbarHidden = true;

            public bool ThrowIfReSortWhenSortComparerIsNull = true;
            public bool AllowSortTopLevelItems = true;

            public bool SupportsTooltips = false;
            public int TooltipDelayMilliseconds = 500;

            public bool SupportsDragSource = false;
            public double DragSourceMovementThreshhold = 3.0;

            public bool UpdateColorsFromThemeWhenDisplayed = true;

            public bool UseRoundedCorners = false;
            public int CornerRadius = 5;

            public bool ShowInteriorDropShadow = false;
            public Color DropShadowColor = Color.Gray;
            public float DropShadowSize = 10;

            public int HorizontalScrollBarHeight = -1;
            public int VerticalScrollBarWidth = -1;

            public bool TreeSupportsClickableExpanders = true;

            public bool Debug_DrawHitboxes = false;

            //when calling SetObjects, instead of re-linking by Item, re-link by Object instead.
            public bool PreserveSelectionByObject = false;

            public bool TreatMiddleMouseLikeLeft = false;
        }

        public class _internal_MultiColumnProperties
        {
            //for multi-column displays
            public bool ColumnHeadersVisible = true;
            //public bool ColumnHeadersSizeable = true;
            public bool ColumnHeadersClickable = true;
            public bool ColumnSizesArePercentages = true;
            public List<NeatListColumn> Columns = new List<NeatListColumn>();
            public Color ColumnHeaderBackgroundColor = Color.LightGray;
            public Color ColumnHeaderBorderColor = Color.Gray;
            //public bool ColumnHeadersCombineBorders = true;
            public float ColumnHeaderBorderThickness = 1.0f;
        }

        public class _internal_GridLayoutProperties
        {
            public int SpacingBetweenRows = 0;
            public int SpacingBetweenColumns = 0;
        }

        public class _internal_Internals
        {
            public NeatListItem HighlightedItem = null;
            public NeatListItem HighlightedItemWhenMouseDown = null;

            public NeatListItem SelectedItem = null;
            public List<NeatListItem> SelectedItems = new List<NeatListItem>();

            public int ScrollX = 0;
            public int ScrollY = 0;
            public int ContentWidth = 0;
            public int ContentHeight = 0;
            public int MaxScrollX = 0;
            public int MaxScrollY = 0;
            public List<NeatListItem> TopLevelItems = new List<NeatListItem>();

            public int SeperatorHeight = -1;
            public int TextLineHeight = -1;
            public int RowHeight = -1;
            public int SeperatorWidth = -1;
            public int ColumnWidth = -1;

            public bool InStateWhereHighlightIsVisible = true; //and also selection

            public bool SuppressRedraw = false;

            public Dictionary<string, Brush> Brushes = new Dictionary<string, Brush>();
            public Dictionary<string, Pen> Pens = new Dictionary<string, Pen>();

            //prevent recalc and redraw while modifying the list
            public bool OutsideCodeModifyingList = false;

            public int ColumnHeaderHeight = 0;

            public Rectangle ContentRect;

            //filled from the list type and item layout prior to calculations
            public int SpacingBetweenRows = 0;
            public int SpacingBetweenColumns = 0;

            public bool MouseInClientArea = false;
            public Point MouseClientPosition;

            public object ThreadSafety = new object();

            public Point MouseDownClientPosition;
            public bool DragDropAlreadyBegun = false; //this is reset to false on each mousedown event; likely will be true much longer than the dragdrop operation takes
            public bool DragDropEventAlreadyFiredThisMouseMove = false; //reset to false on every mousedown/mouseup
        }

        public class _internal_RowColLayoutProperties
        {
            public int SpacingBetweenRows = 0;
            public int SpacingBetweenColumns = 0;
        }

        public class _internal_TreeProperties
        {
            public int SpacingBetweenRows = 0;
            public int SpacingBetweenColumns = 0;
            public int IndentWidth = 20;
            public int ExpanderSize = 16;
            public int MarginBetweenExpanderAndObject = 5;

            public int ExpanderExteriorMargin = 2;
            public int ExpanderInteriorMargin = 3;
            public float ExpanderBorderThickness = 1.0f;
            public Color ExpanderBorderColor = Color.Black;
            public float ExpanderInteriorThickness = 1.0f;
            public Color ExpanderInteriorColor = Color.Black;
            public Color ExpanderBackgroundColor = Color.White;

            public int ObjectInteriorLeftMargin = 5;

            public bool ItemBackgroundCoversWholeRow = true;
        }

        class _internal_CalculateBoundsResults
        {
            public int last_right = 0;
            public int last_bottom = 0;
            public int content_height = 0;
            public int content_width = 0;
            public NeatListItem previous_item;

            public void Reset()
            {
                last_right = 0;
                last_bottom = 0;
                content_height = 0;
                content_width = 0;
                previous_item = null;
            }
        }

        class _internal_PaintInfo
        {
            public bool alt = true;

            public void Reset()
            {
                alt = true;
            }
        }
    }
}
