﻿namespace NeatList
{
    public class NeatListHitTestResults
    {
        public bool RegisteredHit = false;
        public NeatListItem HitItem = null;
        public bool HitExpanderButton = false;
        public bool HitColumnHeader = false;
        public NeatListColumn HitColumn = null;
    }
}
