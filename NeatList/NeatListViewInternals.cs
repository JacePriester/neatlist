﻿using RoundedRectangles;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace NeatList
{
    public partial class NeatListView
    {
        public virtual void BeforePaint(Graphics graphics)
        {
            Internals.Brushes["control_background"] = new SolidBrush(BackColor);
            Internals.Brushes["noicon"] = new SolidBrush(Properties.NoIconColor);
            Internals.Brushes["text"] = new SolidBrush(ForeColor);
            Internals.Brushes["alternate_row_background"] = new SolidBrush(Properties.AlternateRowBackColor);

            Internals.Pens["highlight"] = new Pen(Properties.HighlightColor, Properties.HighlightPenThickness);

            Internals.Brushes["selection"] = new SolidBrush(Properties.SelectionColor);
            Internals.Brushes["selection_while_unfocused"] = new SolidBrush(Properties.SelectionColorWhileUnfocused);

            Internals.Brushes["empty_list_message_background"] = new SolidBrush(Properties.EmptyListBackColor);
            Internals.Pens["empty_list_message_border"] = new Pen(Properties.EmptyListBorderColor, Properties.EmptyListBorderThickness);

            //for multi-column lists
            Internals.Brushes["column_header_background"] = new SolidBrush(MultiColumnProperties.ColumnHeaderBackgroundColor);
            Internals.Pens["column_header_border"] = new Pen(MultiColumnProperties.ColumnHeaderBorderColor, MultiColumnProperties.ColumnHeaderBorderThickness);

            //for trees
            Internals.Brushes["expander_background"] = new SolidBrush(TreeProperties.ExpanderBackgroundColor);
            Internals.Pens["expander_border"] = new Pen(TreeProperties.ExpanderBorderColor, TreeProperties.ExpanderBorderThickness);
            Internals.Pens["expander_interior"] = new Pen(TreeProperties.ExpanderInteriorColor, TreeProperties.ExpanderInteriorThickness);

            //for debug hitboxes
            if (Properties.Debug_DrawHitboxes)
                Internals.Pens["debug_hitbox_boundary"] = new Pen(Color.Magenta, 0.5f);
        }

        public virtual void AfterPaint(Graphics graphics)
        {
            Internals.Brushes.Clear();
            Internals.Pens.Clear();
        }

        public virtual void CalculateLayoutMeasurements()
        {
            if (Internals.OutsideCodeModifyingList)
                return;

            if (Properties.FixedRowHeight != -1)
                Internals.RowHeight = Properties.FixedRowHeight;
            else
                Internals.RowHeight = 30; //guess at it? let derived class do a real calc.

            if (Properties.FixedColumnWidth != -1)
                Internals.ColumnWidth = Properties.FixedColumnWidth;
            else
                Internals.ColumnWidth = 30; //guess at it? let derived class do a real calc.

            Internals.SeperatorHeight = (int)(Internals.RowHeight * Properties.SeperatorHeightScale);
            Internals.SeperatorWidth = (int)(Internals.ColumnWidth * Properties.SeperatorWidthScale);

            if (Properties.ListFormat == NeatListFormat.Simple)
            {
                if (Properties.ItemLayout == NeatItemLayout.GridLeftToRight)
                {
                    Internals.SpacingBetweenRows = GridLayoutProperties.SpacingBetweenRows;
                    Internals.SpacingBetweenColumns = GridLayoutProperties.SpacingBetweenColumns;
                }
                else
                {
                    Internals.SpacingBetweenRows = RowColLayoutProperties.SpacingBetweenRows;
                    Internals.SpacingBetweenColumns = RowColLayoutProperties.SpacingBetweenColumns;
                }
            }
            else if (Properties.ListFormat == NeatListFormat.MultipleColumns)
            {
                Internals.SpacingBetweenRows = 0;
                Internals.SpacingBetweenColumns = 0;
            }
            else if (Properties.ListFormat == NeatListFormat.Tree)
            {
                Internals.SpacingBetweenRows = TreeProperties.SpacingBetweenRows;
                Internals.SpacingBetweenColumns = 0;
            }
        }

        public NeatListView()
        {
            DoubleBuffered = true;

            VisibleChanged += NeatListView_VisibleChanged;

            Properties.EmptyListFont = new Font(Font.FontFamily, Font.Size * 1.5f, FontStyle.Bold);

            Controls.Add(VerticalScroll);
            Controls.Add(HorizontalScroll);

            VerticalScroll.Scroll += VerticalScroll_Scroll;
            VerticalScroll.Cursor = Cursors.Default;

            HorizontalScroll.Scroll += HorizontalScroll_Scroll;
            HorizontalScroll.Cursor = Cursors.Default;
        }

        private void VerticalScroll_Scroll(object sender, ScrollEventArgs e)
        {
            SetScroll(Internals.ScrollX, e.NewValue);
        }

        private void HorizontalScroll_Scroll(object sender, ScrollEventArgs e)
        {
            SetScroll(e.NewValue, Internals.ScrollY);
        }

        protected override void OnLayout(LayoutEventArgs e)
        {
            if (e.AffectedControl == this && e.AffectedProperty == "Bounds")
            {
                UpdateWindowRegion();
                CalculateBounds();
            }
        }

        void UpdateWindowRegion()
        {
            if (Properties.UseRoundedCorners && Properties.CornerRadius > 0)
                Region = new Region(RoundedRectangles.RoundedRectangle.Create(new Rectangle(0, 0, Size.Width, Size.Height), Properties.CornerRadius, RoundedRectangle.RectangleCorners.All, RoundedRectangle.WhichHalf.Both, RoundedRectangle.Purpose.WindowRegion));
            else
                Region = null;
        }

        //outside code should call this after changing rounded corners properties
        public void ModifiedRoundedCorners()
        {
            UpdateWindowRegion();
        }

        private void NeatListView_VisibleChanged(object sender, EventArgs e)
        {
            //if doubleclick is not required, speed up single clicks.
            SetStyle(ControlStyles.StandardDoubleClick, Properties.ItemActivationRequiresDoubleClick);

            if (Properties.UpdateColorsFromThemeWhenDisplayed)
                ColorsFromTheme();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        protected void SmartInvalidate()
        {
            if (Internals.OutsideCodeModifyingList)
                return;

            if (!Internals.SuppressRedraw)
                Invalidate();
        }

        void _HighlightOffset(int offset)
        {
            if (Internals.HighlightedItem == null)
                return;

            lock (Internals.ThreadSafety)
            {
                int current = GetItemIndex(Internals.HighlightedItem);
                if (current == -1)
                    return;
                NeatListItem nextitem = GetItemAtIndex(current + offset);
                if (nextitem != null)
                    SetHighlightedItem(nextitem);
            }
        }

        void _SelectOffset(int offset)
        {
            if (Internals.SelectedItem == null)
                return;

            lock (Internals.ThreadSafety)
            {
                int current = GetItemIndex(Internals.SelectedItem);
                if (current == -1)
                    return;
                NeatListItem nextitem = GetItemAtIndex(current + offset);
                if (nextitem != null)
                    SetSelectedItem(nextitem);
            }
        }

        void CalculateTextLineHeight()
        {
            using (Graphics graphics = Graphics.FromHwnd(Handle))
            {
                graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

                //var fmt = new StringFormat();
                //fmt.Alignment = StringAlignment.Near;
                //fmt.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.NoClip;
                //fmt.LineAlignment = StringAlignment.Near;
                //fmt.Trimming = StringTrimming.None;

                //Internals.TextLineHeight = (int)Math.Ceiling(graphics.MeasureString("'`^ATXgypq,", Font, 100, fmt).Height);
                Internals.TextLineHeight = TextRenderer.MeasureText("'`^ATXgypq,", Font).Height;
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (Properties.FocusOnMouseDown)
                Focus();

            if (e.Button == MouseButtons.Left)
            {
                Internals.DragDropAlreadyBegun = false;
                Internals.DragDropEventAlreadyFiredThisMouseMove = false;
                Internals.HighlightedItemWhenMouseDown = Internals.HighlightedItem;
                Internals.MouseDownClientPosition = e.Location;
            }
        }

        double PointDistance(Point a, Point b)
        {
            double dx = a.X - b.X;
            double dy = a.Y - b.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            Internals.MouseClientPosition = PointToClient(MousePosition);
            Internals.MouseInClientArea = ClientRectangle.Contains(Internals.MouseClientPosition);

            bool redraw = false;
            if (Internals.InStateWhereHighlightIsVisible == false)
            {
                Internals.InStateWhereHighlightIsVisible = true;
                redraw = true;
            }

            bool sub_redraw = CheckMouseHover();
            redraw = redraw || sub_redraw;

            if (redraw) //fixme: checkmousehover  might trigger a repaint, try not to do it twice.
                SmartInvalidate();

            if (Properties.SupportsDragSource)
            {
                if (!Internals.DragDropEventAlreadyFiredThisMouseMove && !Internals.DragDropAlreadyBegun && e.Button == MouseButtons.Left && PointDistance(Internals.MouseClientPosition, Internals.MouseDownClientPosition) >= Properties.DragSourceMovementThreshhold)
                {
                    Internals.DragDropAlreadyBegun = BeginDragDropOperation();
                    Internals.DragDropEventAlreadyFiredThisMouseMove = true;
                }
            }
        }
        
        //return true if dragdrop has begun, otherwise false.
        public virtual bool BeginDragDropOperation()
        {
            //nothing to do! child classes can override.
            return false;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            if (Properties.FocusOnMouseEnter)
                Focus();

            if (Internals.InStateWhereHighlightIsVisible == false)
            {
                Internals.InStateWhereHighlightIsVisible = true;
                SmartInvalidate();
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            Internals.MouseInClientArea = false;

            if (!Properties.ShowHighlightWhenNotFocused || !Properties.KeepHighlightWhenMouseLeavesValidObject)
            {
                Internals.InStateWhereHighlightIsVisible = false;
                SmartInvalidate();
            }
        }

        protected virtual void DrawEmptyListMessage(Graphics graphics)
        {
            //fixme: we should probably precalc some of this stuff, but it isn't too intensive so... mehh

            if (Internals.TopLevelItems.Count == 0 && Properties.EmptyListMessage != null && Properties.EmptyListMessage.Trim() != "")
            {
                Font f = Properties.EmptyListFont != null ? Properties.EmptyListFont : Font;

                //var fmt = new StringFormat();
                //fmt.Alignment = StringAlignment.Near;
                //fmt.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.NoClip;
                //fmt.LineAlignment = StringAlignment.Near;
                //fmt.Trimming = StringTrimming.None;

                //SizeF tsize = graphics.MeasureString(Properties.EmptyListMessage, f, Internals.ContentRect.Width, fmt);
                Size tsize = TextRenderer.MeasureText(Properties.EmptyListMessage, f);

                Rectangle bounds = new Rectangle(Internals.ContentRect.Left + (Internals.ContentRect.Width - tsize.Width) / 2,
                    Internals.ContentRect.Top + (Internals.ContentRect.Height - tsize.Height) / 2,
                    tsize.Width,
                    tsize.Height);

                Point textloc = bounds.Location;

                bounds.Inflate(Properties.EmptyListMessageMargin, Properties.EmptyListMessageMargin);
                GraphicsPath path = RoundedRectangle.Create(bounds, Properties.EmptyListBorderRadius, RoundedRectangle.RectangleCorners.All, RoundedRectangle.WhichHalf.Both, RoundedRectangle.Purpose.Drawing);
                graphics.FillPath(Internals.Brushes["empty_list_message_background"], path);
                graphics.DrawPath(Internals.Pens["empty_list_message_border"], path);

                TextRenderer.DrawText(graphics, Properties.EmptyListMessage, f, textloc, ForeColor);
                //graphics.DrawString(Properties.EmptyListMessage, f, Internals.Brushes["text"], textloc, fmt);
            }
        }

        void PaintForChildren(PaintEventArgs e, NeatListItem ContainerItem, bool backgrounds, bool hitboxes)
        {
            List<NeatListItem> items = (ContainerItem == null) ? Internals.TopLevelItems : ContainerItem.ChildItems;

            foreach (NeatListItem item in items)
            {
                PaintInfo.alt = !PaintInfo.alt;

                if (item.ScrolledBounds.IntersectsWith(e.ClipRectangle))
                {
                    bool is_selected = Internals.SelectedItems.Contains(item);
                    bool is_highlighted = (Internals.HighlightedItem == item);

                    if (backgrounds)
                    {
                        item.DrawBackground(e.Graphics, PaintInfo.alt);

                        if (Properties.SupportsSelection && Properties.DrawSelectionBehindObject)
                            item.DrawSelection(e.Graphics, PaintInfo.alt, is_selected);
                    }
                    else if (hitboxes)
                    {
                        e.Graphics.DrawRectangle(Internals.Pens["debug_hitbox_boundary"], item.ScrolledBounds);
                    }
                    else
                    {
                        item.Draw(e.Graphics, PaintInfo.alt);

                        if (Properties.SupportsSelection && !Properties.DrawSelectionBehindObject)
                            item.DrawSelection(e.Graphics, PaintInfo.alt, is_selected);

                        if (Properties.SupportsHighlight && Internals.InStateWhereHighlightIsVisible)
                            item.DrawHighlight(e.Graphics, PaintInfo.alt, is_highlighted);

                        if (Properties.ListFormat == NeatListFormat.Tree && Properties.TreeSupportsClickableExpanders)
                            item.DrawExpander(e.Graphics);
                    }
                }
                
                if (Properties.ListFormat == NeatListFormat.Tree && item.IsExpanded)
                    PaintForChildren(e, item, backgrounds, hitboxes);
            }
        }

        public virtual void PaintBackground(Graphics g)
        {
            g.FillRectangle(Internals.Brushes["control_background"], ClientRectangle);
        }

        public virtual void PaintInteriorDropShadow(Graphics g)
        {
            if (Properties.ShowInteriorDropShadow)
            {
                using (Brush aGradientBrush = new LinearGradientBrush(new PointF(0, 0), new PointF(0, (float)Math.Ceiling(Properties.DropShadowSize / 2.0f) + 1.0f), Properties.DropShadowColor, Color.Transparent))
                {
                    using (Pen aGradientPen = new Pen(aGradientBrush, Properties.DropShadowSize))
                    {
                        g.DrawLine(aGradientPen, 0, 0, ClientRectangle.Width, 0);
                    }
                }

                using (Brush aGradientBrush = new LinearGradientBrush(new PointF(0, 0), new PointF((float)Math.Ceiling(Properties.DropShadowSize / 2.0f) + 1.0f, 0), Properties.DropShadowColor, Color.Transparent))
                {
                    using (Pen aGradientPen = new Pen(aGradientBrush, Properties.DropShadowSize))
                    {
                        g.DrawLine(aGradientPen, 0, 0, 0, ClientRectangle.Height);
                    }
                }
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            lock (Internals.ThreadSafety)
            {
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                //e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
                e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                BeforePaint(e.Graphics);

                //control background color
                PaintBackground(e.Graphics);

                DrawEmptyListMessage(e.Graphics); //if empty, will draw it

                PaintInfo.Reset();
                PaintForChildren(e, null, true, false); //backgrounds
                PaintInteriorDropShadow(e.Graphics);
                PaintForChildren(e, null, false, false); //actual objects
                if(Properties.Debug_DrawHitboxes)
                    PaintForChildren(e, null, false, true); //draw hitboxes

                if (Properties.ListFormat == NeatListFormat.MultipleColumns && MultiColumnProperties.ColumnHeadersVisible)
                {
                    foreach (NeatListColumn col in MultiColumnProperties.Columns)
                        col.Draw(e.Graphics);
                }

                AfterPaint(e.Graphics);
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            if (Internals.InStateWhereHighlightIsVisible == false)
            {
                Internals.InStateWhereHighlightIsVisible = true;
            }
            
            SmartInvalidate();
        }

        protected override void OnLostFocus(EventArgs e)
        {
            if (!Properties.ShowHighlightWhenNotFocused)
            {
                Internals.InStateWhereHighlightIsVisible = false;
            }
            
            SmartInvalidate();
        }

        void HitTestForChildren(NeatListItem ContainerItem, ref NeatListHitTestResults hitres, int x, int y)
        {
            List<NeatListItem> items = (ContainerItem == null) ? Internals.TopLevelItems : ContainerItem.ChildItems;

            //bug alert! if we have overlapping items, hittest returns on the first item it matches.
            //problem is that this will be the first item drawn, therefore the bottom layer.
            //instead we need to loop in reverse here so we return on the top layer.
            //in non-overlapping cases this has zero effect.

            for (int i = items.Count - 1; i >= 0; i -= 1)
            {
                NeatListItem item = items[i];

                if (Properties.ListFormat == NeatListFormat.Tree && Properties.TreeSupportsClickableExpanders && item.GetExpanderHitBounds().Contains(x, y))
                {
                    hitres.HitItem = item;
                    hitres.HitExpanderButton = true;
                    hitres.RegisteredHit = true;
                    return;
                }
                
                if (Properties.ListFormat == NeatListFormat.Tree && item.IsExpanded)
                {
                    HitTestForChildren(item, ref hitres, x, y);
                    if (hitres.RegisteredHit)
                        return;
                }

                if (item.IsHittable && item.GetHitBounds().Contains(x, y))
                {
                    if (item.TheObject == null) //we dont count seperator hits
                        break;
                    hitres.HitItem = item;
                    hitres.RegisteredHit = true;
                    return;
                }
            }
        }

        //returns true if redraw required
        bool CheckMouseHover()
        {
            lock (Internals.ThreadSafety)
            {
                NeatListHitTestResults hitres = Internals.MouseInClientArea ? HitTest(Internals.MouseClientPosition.X, Internals.MouseClientPosition.Y) : null;

                NeatListItem interior_item = (hitres != null && hitres.HitItem != null && !hitres.HitExpanderButton) ? hitres.HitItem : null;

                if (Properties.CursorHandOnClickableItem)
                {
                    bool cursorhand = false;
                    if (interior_item != null && interior_item.IsUserActivatable)
                        cursorhand = true;

                    if (InvokeRequired)
                        BeginInvoke(new MethodInvoker(() => { Cursor = cursorhand ? Cursors.Hand : Cursors.Default; }));
                    else
                        Cursor = cursorhand ? Cursors.Hand : Cursors.Default;
                }

                if (interior_item == Internals.HighlightedItem) //no change
                    return false;

                if (interior_item == null && Properties.KeepHighlightWhenMouseLeavesValidObject) //consider that no change
                    return false;

                if(Properties.SupportsHighlight && (interior_item == null || interior_item.IsUserHighlightable))
                    SetHighlightedItem(interior_item);
                
                return true;
            }
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            lock (Internals.ThreadSafety)
            {
                if (Properties.ItemActivationRequiresDoubleClick)
                {
                    NeatListHitTestResults hitres = HitTest(e.X, e.Y);

                    if (hitres.HitItem != null && hitres.HitItem.TheObject != null && hitres.HitItem.IsUserActivatable)
                    {
                        ItemActivate?.Invoke(this, hitres.HitItem);
                    }
                }
            }
        }

        bool CompareListsWithoutOrder<T>(List<T> list1, List<T> list2)
        {
            var cnt = new Dictionary<T, int>();
            foreach (T s in list1)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]++;
                }
                else
                {
                    cnt.Add(s, 1);
                }
            }
            foreach (T s in list2)
            {
                if (cnt.ContainsKey(s))
                {
                    cnt[s]--;
                }
                else
                {
                    return false;
                }
            }
            return cnt.Values.All(c => c == 0);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (Internals.DragDropAlreadyBegun) //dont do anything if we did a drag-drop
                return;

            lock (Internals.ThreadSafety)
            {
                NeatListHitTestResults hitres = HitTest(e.X, e.Y);

                if (e.Button == MouseButtons.Left || (Properties.TreatMiddleMouseLikeLeft && e.Button == MouseButtons.Middle))
                {
                    if (Properties.ListFormat == NeatListFormat.Tree && hitres.HitExpanderButton)
                    {
                        if (hitres.HitItem != null)
                            ToggleExpanded(hitres.HitItem);
                    }
                    else if (Properties.ListFormat == NeatListFormat.MultipleColumns && hitres.HitColumnHeader)
                    {
                        ColumnHeaderClick?.Invoke(this, hitres.HitColumn);
                    }
                    else
                    {
                        if (Properties.SupportsSelection)
                        {
                            //selection modes are:
                            //0 = single select, 1 = multi select another single item, 2 = multi range select

                            //backup current selection
                            NeatListItem prev_SelectedItem = Internals.SelectedItem;
                            List<NeatListItem> prev_SelectedItems = new List<NeatListItem>();
                            foreach (NeatListItem item in Internals.SelectedItems)
                                prev_SelectedItems.Add(item);

                            int mode = -1;

                            if (!Properties.SupportsMultiSelect)
                                mode = 0;
                            else
                            {
                                if (ModifierKeys.HasFlag(Keys.Control))
                                    mode = 1;
                                else if (ModifierKeys.HasFlag(Keys.Shift) && Properties.TreatShiftLikeControl)
                                    mode = 1;
                                else if (ModifierKeys.HasFlag(Keys.Shift))
                                {
                                    if (Internals.SelectedItem == null)
                                        mode = 1;
                                    else
                                        mode = 2;
                                }
                                else
                                    mode = 0;
                            }

                            if (mode == 0)
                            {
                                if (hitres.HitItem == null || hitres.HitItem.TheObject == null || (hitres.HitItem != null && !hitres.HitItem.IsUserSelectable))
                                {
                                    if (!Properties.KeepAtLeastOneItemSelected)
                                    {
                                        Internals.SelectedItem = null;
                                        Internals.SelectedItems.Clear();
                                        SmartInvalidate();
                                    }
                                }
                                else
                                {
                                    Internals.SelectedItem = hitres.HitItem;
                                    Internals.SelectedItems.Clear();
                                    Internals.SelectedItems.Add(hitres.HitItem);
                                    SmartInvalidate();
                                }
                            }
                            else if (mode == 1)
                            {
                                if (hitres.HitItem == null || hitres.HitItem.TheObject == null || (hitres.HitItem != null && !hitres.HitItem.IsUserSelectable))
                                {
                                    //nothing to do?
                                }
                                else
                                {
                                    if (!Internals.SelectedItems.Contains(hitres.HitItem))
                                    {
                                        Internals.SelectedItems.Add(hitres.HitItem);
                                        Internals.SelectedItem = hitres.HitItem;
                                        SmartInvalidate();
                                    }
                                    else
                                    {
                                        if (Internals.SelectedItems.Count >= 2 || !Properties.KeepAtLeastOneItemSelected)
                                        {
                                            Internals.SelectedItems.Remove(hitres.HitItem);
                                            Internals.SelectedItem = null;
                                            SmartInvalidate();
                                        }
                                    }
                                }
                            }
                            else if (mode == 2)
                            {
                                if (hitres.HitItem == null || hitres.HitItem.TheObject == null || (hitres.HitItem != null && !hitres.HitItem.IsUserSelectable))
                                {
                                    //nothing to do?
                                }
                                else
                                {
                                    if (Internals.SelectedItem == null) //nothing prior, pretty much the same as mode 1
                                    {
                                        Internals.SelectedItem = hitres.HitItem;
                                        if (!Internals.SelectedItems.Contains(hitres.HitItem))
                                            Internals.SelectedItems.Add(hitres.HitItem);
                                        SmartInvalidate();
                                    }
                                    else //the range select, the really neat part
                                    {
                                        int lastindex = GetItemIndex(Internals.SelectedItem);
                                        int newindex = GetItemIndex(hitres.HitItem);

                                        int min = (int)Math.Min(lastindex, newindex);
                                        int max = (int)Math.Max(lastindex, newindex);

                                        for (int i = min; i <= max; i += 1)
                                        {
                                            NeatListItem thisitem = GetItemAtIndex(i);
                                            if (!Internals.SelectedItems.Contains(thisitem) && thisitem.IsUserSelectable)
                                                Internals.SelectedItems.Add(thisitem);
                                        }

                                        Internals.SelectedItem = hitres.HitItem;

                                        SmartInvalidate();
                                    }

                                    SmartInvalidate();
                                }
                            }

                            NotifyIfSelectionChanged(prev_SelectedItem, prev_SelectedItems);
                        }

                        if (Properties.SupportsItemActivation && !Properties.ItemActivationRequiresDoubleClick)
                        {
                            if (hitres.HitItem != null && hitres.HitItem.TheObject != null && hitres.HitItem.IsUserActivatable)
                            {
                                if (e.Button == MouseButtons.Left)
                                    ItemActivate?.Invoke(this, hitres.HitItem);
                                else
                                    ItemActivateMiddleButton?.Invoke(this, hitres.HitItem);
                            }
                        }
                    }
                }
                else if (e.Button == MouseButtons.Right)
                {
                    if (hitres.HitItem != null && hitres.HitItem.TheObject != null)
                        ItemRightClick?.Invoke(this, hitres.HitItem);
                    else if (Properties.ListFormat == NeatListFormat.MultipleColumns && hitres.HitColumnHeader)
                    {
                        ColumnHeaderRightClick?.Invoke(this, hitres.HitColumn);
                    }
                    else
                        NonItemRightClick?.Invoke(this, null);
                }
            }
        }

        void NotifyIfSelectionChanged(NeatListItem prev_SelectedItem, List<NeatListItem> prev_SelectedItems)
        {
            bool changes_made = false;
            if (Properties.SupportsMultiSelect)
                changes_made = CompareListsWithoutOrder<NeatListItem>(prev_SelectedItems, Internals.SelectedItems);
            else
                changes_made = Internals.SelectedItem != prev_SelectedItem;

            if (changes_made)
                SelectionChanged?.Invoke(this, Internals.SelectedItem);
        }

        void CalculateColumnHeaderBounds()
        {
            Internals.ColumnHeaderHeight = 0;

            if (Properties.ListFormat != NeatListFormat.MultipleColumns)
                return;

            List<int> calced_widths = new List<int>();

            int total_columns_width = 0;
            foreach (NeatListColumn col in MultiColumnProperties.Columns)
            {
                if (!col.SizeFillsRemainingSpace)
                {
                    int w = MultiColumnProperties.ColumnSizesArePercentages ? ((int)(col.Size / 100.0 * ClientRectangle.Width)) : (int)col.Size;
                    calced_widths.Add(w);

                    total_columns_width += w;
                }
            }

            int remaining = ClientRectangle.Width - total_columns_width;
            if (remaining < 0)
                remaining = 0;

            NeatListColumn previous = null;
            for (int i = 0, j = 0; i < MultiColumnProperties.Columns.Count; i += 1)
            {
                NeatListColumn col = MultiColumnProperties.Columns[i];

                if (!col.SizeFillsRemainingSpace)
                {
                    col.CalculateHeaderBounds(previous, calced_widths[j]);
                    j += 1;
                }
                else
                    col.CalculateHeaderBounds(previous, remaining);

                Internals.ColumnHeaderHeight = (int)Math.Max(Internals.ColumnHeaderHeight, col.Bounds.Height);

                previous = col;
            }

            if (MultiColumnProperties.ColumnHeadersVisible)
            {
                Internals.ContentRect.Y += Internals.ColumnHeaderHeight;
                Internals.ContentRect.Height -= Internals.ColumnHeaderHeight;
            }
        }

        void SetParentingForChildren(NeatListItem ContainerItem)
        {
            List<NeatListItem> items = (ContainerItem == null) ? Internals.TopLevelItems : ContainerItem.ChildItems;

            foreach (NeatListItem item in items)
            {
                //this allows outside code to directly modify lists without having to set this stuff. we can do it here!
                item.ParentItem = ContainerItem;
                item.ParentListView = this;

                SetParentingForChildren(item);
            }
        }

        void CalculateCanExpandCollapseForChildren(NeatListItem ContainerItem)
        {
            List<NeatListItem> items = (ContainerItem == null) ? Internals.TopLevelItems : ContainerItem.ChildItems;

            foreach (NeatListItem item in items)
            {
                item.EvaluateCanExpand();
                item.EvaluateCanCollapse();

                CalculateCanExpandCollapseForChildren(item);
            }
        }

        void CalculateBoundsForChildren(NeatListItem ContainerItem)
        {
            List<NeatListItem> items = (ContainerItem == null) ? Internals.TopLevelItems : ContainerItem.ChildItems;

            foreach (NeatListItem item in items)
            {
                item.CalculateBounds(CalculateBoundsResults.previous_item);
                CalculateBoundsResults.last_right = item.Bounds.Right;
                CalculateBoundsResults.last_bottom = item.Bounds.Bottom;
                CalculateBoundsResults.content_height = (int)Math.Max(CalculateBoundsResults.content_height, CalculateBoundsResults.last_bottom);
                CalculateBoundsResults.content_width = (int)Math.Max(CalculateBoundsResults.content_width, CalculateBoundsResults.last_right);
                CalculateBoundsResults.previous_item = item;

                if (Properties.ListFormat == NeatListFormat.Tree && item.IsExpanded)
                    CalculateBoundsForChildren(item);
            }
        }

        void CalculateExpanderBoundsForChildren(NeatListItem ContainerItem)
        {
            List<NeatListItem> items = (ContainerItem == null) ? Internals.TopLevelItems : ContainerItem.ChildItems;

            if (ContainerItem == null || ContainerItem.IsExpanded)
            {
                foreach (NeatListItem item in items)
                {
                    item.CalculateExpanderBounds_AdjustPrimaryBounds();

                    CalculateExpanderBoundsForChildren(item);
                }
            }
        }

        void CalculateBounds()
        {
            if (Internals.OutsideCodeModifyingList)
                return;

            
            lock (Internals.ThreadSafety)
            {
                Internals.SuppressRedraw = true;

                Internals.ContentRect = ClientRectangle;
                if (VerticalScroll.Visible || !Properties.ExpandContentWhenScrollbarHidden)
                    Internals.ContentRect.Width -= VerticalScroll.Width;

                if (HorizontalScroll.Visible || !Properties.ExpandContentWhenScrollbarHidden)
                    Internals.ContentRect.Height -= HorizontalScroll.Height;

                if (Internals.TextLineHeight == -1)
                    CalculateTextLineHeight();

                CalculateLayoutMeasurements();

                CalculateColumnHeaderBounds();

                CalculateBoundsResults.Reset();

                SetParentingForChildren(null);
                CalculateBoundsForChildren(null);

                if (Properties.ListFormat == NeatListFormat.Tree)
                {
                    if (Properties.TreeSupportsClickableExpanders)
                    {
                        CalculateCanExpandCollapseForChildren(null);
                        CalculateExpanderBoundsForChildren(null);
                    }
                }

                Internals.ContentWidth = CalculateBoundsResults.content_width;
                Internals.ContentHeight = CalculateBoundsResults.content_height;

                Internals.MaxScrollY = Internals.ContentHeight - Internals.ContentRect.Height;
                if (Internals.MaxScrollY < 0)
                    Internals.MaxScrollY = 0;

                Internals.MaxScrollX = Internals.ContentWidth - Internals.ContentRect.Width;
                if (Internals.MaxScrollX < 0)
                    Internals.MaxScrollX = 0;

                CalculateScrolledBounds();
                CheckMouseHover();

                InvokeUpdateScrollbars();

                Internals.SuppressRedraw = false;
            }

            SmartInvalidate();
        }

        void InvokeUpdateScrollbars()
        {
            if (InvokeRequired)
                BeginInvoke(new MethodInvoker(() => { InvokeUpdateScrollbars(); })); //invoke, or begininvoke? i think begin, really no NEED to do it right now.
            else
            {
                VerticalScroll.Size = new Size(Properties.VerticalScrollBarWidth == -1 ? SystemInformation.VerticalScrollBarWidth: Properties.VerticalScrollBarWidth, ClientRectangle.Height);
                VerticalScroll.Location = new Point(ClientRectangle.Width - VerticalScroll.Width, 0);

                HorizontalScroll.Size = new Size(ClientRectangle.Width, Properties.HorizontalScrollBarHeight == -1 ? SystemInformation.HorizontalScrollBarHeight : Properties.HorizontalScrollBarHeight);
                HorizontalScroll.Location = new Point(0, ClientRectangle.Height - HorizontalScroll.Height);

                bool was_vert_visible = VerticalScroll.Visible;
                bool was_horz_visible = HorizontalScroll.Visible;

                int ymax = Internals.MaxScrollY + Internals.ContentRect.Height + 1;
                int hmax = Internals.MaxScrollX + Internals.ContentRect.Width + 1;

                VerticalScroll.Maximum = ymax >= 0 ? ymax : 0;
                VerticalScroll.LargeChange = Internals.ContentRect.Height >= 0 ? Internals.ContentRect.Height : 0;

                HorizontalScroll.Maximum = hmax >= 0 ? hmax : 0;
                HorizontalScroll.LargeChange = Internals.ContentRect.Width >= 0 ? Internals.ContentRect.Width : 0;

                if (Properties.EnableVerticalScroll)
                    VerticalScroll.Visible = Internals.ContentHeight > Internals.ContentRect.Height;
                else
                    VerticalScroll.Visible = false;

                if (Properties.EnableHorizontalScroll)
                    HorizontalScroll.Visible = Internals.ContentWidth > Internals.ContentRect.Width;
                else
                    HorizontalScroll.Visible = false;

                if (VerticalScroll.Visible != was_vert_visible || HorizontalScroll.Visible != was_horz_visible)
                    CalculateBounds();
            }
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            double nlines = (e.Delta / 120.0) * SystemInformation.MouseWheelScrollLines;

            //prefer vertical scroll with mousewheel if available
            if (Properties.EnableVerticalScroll)
            {
                int pixels = (int)(nlines * Internals.RowHeight);
                SetScroll(Internals.ScrollX, Internals.ScrollY - pixels);
            }
            else if (Properties.EnableHorizontalScroll)
            {
                int pixels = (int)(nlines * Internals.ColumnWidth);
                SetScroll(Internals.ScrollX - pixels, Internals.ScrollY);
            }
        }

        void CalculateScrolledBoundsForChildren(NeatListItem ContainerItem)
        {
            List<NeatListItem> items = (ContainerItem == null) ? Internals.TopLevelItems : ContainerItem.ChildItems;

            foreach (NeatListItem item in items)
            {
                item.CalculateScroll();
                CalculateScrolledBoundsForChildren(item);
            }
        }

        void CalculateScrolledBounds()
        {
            if (Internals.OutsideCodeModifyingList)
                return;

            lock (Internals.ThreadSafety)
            {
                if (Internals.ScrollY < 0)
                    Internals.ScrollY = 0;

                if (Internals.ScrollY > Internals.MaxScrollY)
                    Internals.ScrollY = Internals.MaxScrollY;

                if (Internals.ScrollX < 0)
                    Internals.ScrollX = 0;

                if (Internals.ScrollX > Internals.MaxScrollX)
                    Internals.ScrollX = Internals.MaxScrollX;

                if (VerticalScroll.Value != Internals.ScrollY || HorizontalScroll.Value != Internals.ScrollX)
                {
                    if (InvokeRequired)
                    {
                        BeginInvoke(new MethodInvoker(() =>
                        {
                            VerticalScroll.Value = Internals.ScrollY;
                            HorizontalScroll.Value = Internals.ScrollX;
                        }));
                    }
                    else
                    {
                        VerticalScroll.Value = Internals.ScrollY;
                        HorizontalScroll.Value = Internals.ScrollX;
                    }
                }

                CalculateScrolledBoundsForChildren(null);
            }
        }
        

        void StartTooltipTimeout()
        {
            if (Properties.SupportsTooltips)
            {
                if (InvokeRequired)
                    Invoke(new MethodInvoker(() => { StartTooltipTimeout(); }));
                else
                {
                    ToolTip.Hide(this);
                    TooltipTimer.Stop();
                    
                    TooltipTimer = new Timer();
                    TooltipTimer.Interval = Properties.TooltipDelayMilliseconds;
                    TooltipTimer.Tick += new EventHandler(Tooltip_timer_Tick);
                    TooltipTimer.Start();
                }
            }
        }

        private void Tooltip_timer_Tick(object sender, EventArgs e)
        {
            Timer mytimer = (Timer)sender;
            mytimer.Stop();
            
            //make a copy of the item so we can do this without having to lock for thread safety.
            NeatListItem item = Internals.HighlightedItem;
            if (item != null)
            {
                //also ensure it's highlighted because we are actually hovering it
                if (Internals.MouseInClientArea && item.ScrolledBounds.Contains(Internals.MouseClientPosition))
                {
                    item.ShowTooltip();
                }
            }
        }
    }
}
