﻿using RoundedRectangles;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace NeatList
{
    public abstract class NeatListItem
    {
        public int ID = 0; //an ID that outside code can use to ID an object
        public object TheObject;
        public NeatListView ParentListView;

        //unscrolled bounds
        public Rectangle Bounds; //bounds of item. items can calculate this themselves.

        //real client space with scroll applied; items do NOT need to set this themselves, only bounds above.
        public Rectangle ScrolledBounds;

        //for tree views
        public bool IsExpanded = false;
        public bool CanExpand = true;
        public bool CanCollapse = true;
        public List<NeatListItem> ChildItems = new List<NeatListItem>();
        public NeatListItem ParentItem = null;
        public bool AllowSortChildren = true;

        public bool IsUserSelectable = true;
        public bool IsUserHighlightable = true;
        public bool IsUserActivatable = true;
        public bool IsHittable = true;

        //for drawing
        //public Rectangle ExpanderBounds;
        //public Rectangle ScrolledExpanderBounds;

        //for hit testing
        public Rectangle ExpanderButtonBounds;
        public Rectangle ScrolledExpanderButtonBounds;

        public NeatListItem()
        {
        }

        public NeatListItem(object obj)
        {
            this.TheObject = obj;
        }

        public NeatListItem(object obj, int ID)
        {
            this.TheObject = obj;
            this.ID = ID;
        }

        public virtual Rectangle GetHitBounds()
        {
            //normally just the scrolled bounds. can return a larger or smaller area if you like by overriding.
            return ScrolledBounds;
        }

        //for tree views
        public virtual Rectangle GetExpanderHitBounds()
        {
            //normally just the scrolled bounds.
            return ScrolledExpanderButtonBounds;
        }

        public virtual void DrawExpander(Graphics graphics)
        {
            Rectangle r = ScrolledExpanderButtonBounds;
            r.Inflate(-ParentListView.TreeProperties.ExpanderExteriorMargin, -ParentListView.TreeProperties.ExpanderExteriorMargin);

            graphics.FillRectangle(ParentListView.Internals.Brushes["expander_background"], r);
            graphics.DrawRectangle(ParentListView.Internals.Pens["expander_border"], r);

            r.Inflate(-ParentListView.TreeProperties.ExpanderInteriorMargin, -ParentListView.TreeProperties.ExpanderInteriorMargin);
            
            graphics.DrawLine(ParentListView.Internals.Pens["expander_interior"],
                new Point(r.Left, r.Top + r.Height / 2),
                new Point(r.Right, r.Top + r.Height / 2));

            if (!IsExpanded)
            {
                graphics.DrawLine(ParentListView.Internals.Pens["expander_interior"],
                    new Point(r.Left + r.Width / 2, r.Top),
                    new Point(r.Left + r.Width / 2, r.Bottom));
            }
        }

        public virtual void DrawBackground(Graphics graphics, bool is_alternate_row)
        {
            if (is_alternate_row)
            {
                Rectangle bgr = ScrolledBounds;

                if (ParentListView.Properties.ListFormat == NeatListFormat.Tree && ParentListView.TreeProperties.ItemBackgroundCoversWholeRow)
                {
                    bgr = new Rectangle(ParentListView.Internals.ContentRect.Left,
                        ScrolledBounds.Top,
                        ParentListView.Internals.ContentRect.Width,
                        ScrolledBounds.Height);
                }

                graphics.FillRectangle(ParentListView.Internals.Brushes["alternate_row_background"], bgr);
            }
        }

        public virtual void Draw(Graphics graphics, bool is_alternate_row)
        {
        }
        
        public virtual void CalculateScroll()
        {
            ScrolledBounds = Bounds;
            ScrolledBounds.Offset(-ParentListView.Internals.ScrollX, - ParentListView.Internals.ScrollY);

            //ScrolledExpanderBounds = ExpanderBounds;
            //ScrolledExpanderBounds.Offset(0, -ParentListView.Internals.scroll_y);

            ScrolledExpanderButtonBounds = ExpanderButtonBounds;
            ScrolledExpanderButtonBounds.Offset(-ParentListView.Internals.ScrollX, -ParentListView.Internals.ScrollY);
        }
        
        public virtual void DrawSelection(Graphics graphics, bool is_alternate_row, bool is_selected)
        {
            if (is_selected)
            {
                if (ParentListView.Properties.ShowSelectionWhenNotFocused || ParentListView.Focused)
                {
                    Brush brush = ParentListView.Internals.Brushes["selection"];
                    if (!ParentListView.Focused && ParentListView.Properties.ShowSelectionAlternateStyleWhenNotFocused)
                        brush = ParentListView.Internals.Brushes["selection_while_unfocused"];

                    Rectangle selection_bounds = this.ScrolledBounds;
                    selection_bounds.Inflate(-1, -1);

                    GraphicsPath path = RoundedRectangle.Create(selection_bounds, 7, RoundedRectangle.RectangleCorners.All, RoundedRectangle.WhichHalf.Both, RoundedRectangle.Purpose.Drawing);
                    graphics.FillPath(brush, path);
                }
            }
        }

        public virtual void DrawHighlight(Graphics graphics, bool is_alternate_row, bool is_highlighted)
        {
            if (is_highlighted)
            {
                Rectangle highlight_bounds = this.ScrolledBounds;
                highlight_bounds.Inflate(-1, -1);
                
                GraphicsPath path = RoundedRectangle.Create(highlight_bounds, 7, RoundedRectangle.RectangleCorners.All, RoundedRectangle.WhichHalf.Both, RoundedRectangle.Purpose.Drawing);
                graphics.DrawPath(ParentListView.Internals.Pens["highlight"], path);
            }
        }

        public virtual void CalculateExpanderBounds_AdjustPrimaryBounds()
        {
            int level = 0;
            NeatListItem item = this;
            while (item.ParentItem != null)
            {
                item = item.ParentItem;
                level += 1;
            }

            int indent = ParentListView.TreeProperties.IndentWidth * level;

            int exsize = ParentListView.TreeProperties.ExpanderSize;
            ExpanderButtonBounds = new Rectangle(indent, Bounds.Top + (Bounds.Height - exsize) / 2, exsize, exsize);

            int mainbojectoffset = indent + exsize + ParentListView.TreeProperties.MarginBetweenExpanderAndObject;
            Bounds.X += mainbojectoffset;
            Bounds.Width -= mainbojectoffset;
        }

        public virtual void CalculateBounds(NeatListItem previous)
        {
            if (ParentListView.Properties.ListFormat == NeatListFormat.MultipleColumns)
            {
                Bounds = new Rectangle(ParentListView.Internals.ContentRect.Left + ParentListView.Properties.LeftMargin,
                        previous == null ? ParentListView.Internals.ContentRect.Top + ParentListView.Properties.TopMargin : previous.Bounds.Bottom + ParentListView.Internals.SpacingBetweenRows,
                        ParentListView.Internals.ContentRect.Width,
                        this.TheObject == null ? ParentListView.Internals.SeperatorHeight : ParentListView.Internals.RowHeight);
            }
            else if (ParentListView.Properties.ListFormat == NeatListFormat.Tree)
            {
                //copypasta from the simple row style
                Bounds = new Rectangle(ParentListView.Internals.ContentRect.Left + ParentListView.Properties.LeftMargin,
                    previous == null ? ParentListView.Internals.ContentRect.Top + ParentListView.Properties.TopMargin : previous.Bounds.Bottom + ParentListView.Internals.SpacingBetweenRows,
                    ParentListView.Internals.ContentRect.Width,
                    this.TheObject == null ? ParentListView.Internals.SeperatorHeight : ParentListView.Internals.RowHeight);
            }
            else if (ParentListView.Properties.ListFormat == NeatListFormat.Simple)
            {
                if (ParentListView.Properties.ItemLayout == NeatItemLayout.Rows)
                {
                    Bounds = new Rectangle(ParentListView.Internals.ContentRect.Left + ParentListView.Properties.LeftMargin,
                        previous == null ? ParentListView.Internals.ContentRect.Top + ParentListView.Properties.TopMargin : previous.Bounds.Bottom + ParentListView.Internals.SpacingBetweenRows,
                        ParentListView.Internals.ContentRect.Width,
                        this.TheObject == null ? ParentListView.Internals.SeperatorHeight : ParentListView.Internals.RowHeight);
                }
                else if (ParentListView.Properties.ItemLayout == NeatItemLayout.Columns)
                {
                    Bounds = new Rectangle(previous == null ? ParentListView.Internals.ContentRect.Left + ParentListView.Properties.LeftMargin : previous.Bounds.Right + ParentListView.Internals.SpacingBetweenColumns,
                        ParentListView.Internals.ContentRect.Top + ParentListView.Properties.TopMargin,
                        this.TheObject == null ? ParentListView.Internals.SeperatorWidth : ParentListView.Internals.ColumnWidth,
                        ParentListView.Internals.ContentRect.Height);
                }
                else if (ParentListView.Properties.ItemLayout == NeatItemLayout.GridLeftToRight)
                {
                    int x = ParentListView.Internals.ContentRect.Left + ParentListView.Properties.LeftMargin;
                    int y = ParentListView.Internals.ContentRect.Top + ParentListView.Properties.TopMargin;

                    if (previous != null)
                    {
                        x = previous.Bounds.Right + ParentListView.Internals.SpacingBetweenColumns;
                        y = previous.Bounds.Top;
                        if (x + ParentListView.Internals.ColumnWidth + ParentListView.Properties.RightMargin >= ParentListView.Internals.ContentRect.Right)
                        {
                            x = ParentListView.Internals.ContentRect.Left + ParentListView.Properties.LeftMargin;
                            y += ParentListView.Internals.RowHeight + ParentListView.Internals.SpacingBetweenRows;
                        }
                    }

                    Bounds = new Rectangle(x, y, ParentListView.Internals.ColumnWidth, ParentListView.Internals.RowHeight);
                }
                else if (ParentListView.Properties.ItemLayout == NeatItemLayout.GridTopToBottom)
                {
                    int x = ParentListView.Internals.ContentRect.Left + ParentListView.Properties.LeftMargin;
                    int y = ParentListView.Internals.ContentRect.Top + ParentListView.Properties.TopMargin;

                    if (previous != null)
                    {
                        x = previous.Bounds.Left;
                        y = previous.Bounds.Bottom + ParentListView.Internals.SpacingBetweenRows;
                        if (y + ParentListView.Internals.RowHeight + ParentListView.Properties.BottomMargin >= ParentListView.Internals.ContentRect.Bottom)
                        {
                            x += ParentListView.Internals.ColumnWidth + ParentListView.Internals.SpacingBetweenColumns;
                            y = ParentListView.Internals.ContentRect.Top + ParentListView.Properties.TopMargin;
                        }
                    }

                    Bounds = new Rectangle(x, y, ParentListView.Internals.ColumnWidth, ParentListView.Internals.RowHeight);
                }
            }
        }

        public virtual void EvaluateCanExpand()
        {
            //subclasses should do an actual evaluation somehow. for a generic item we don't care.
            CanExpand = ChildItems.Count > 0;
        }

        public virtual void EvaluateCanCollapse()
        {
            //subclasses should do an actual evaluation somehow. for a generic item we don't care.
            CanCollapse = true;
        }

        //called even if the item has CanExpand = false
        public virtual bool BeforeExpand()
        {
            //return false to cancel the expand
            return true;
        }

        //called even if the item has CanCollapse = false
        public virtual bool BeforeCollapse()
        {
            //return false to cancel the collapse
            return true;
        }

        public virtual void OnExpand()
        {
        }

        public virtual void OnCollapse()
        {
        }

        public virtual void ShowTooltip()
        {
        }
    };
}
