﻿using System.Drawing;
using NeatList;

namespace NeatList
{
    class IconListItem : NeatListItem
    {
        Rectangle IconBounds_Local;

        public IconListItem() { }
        public IconListItem(Bitmap obj) { this.TheObject = obj; }
        
        public override void Draw(Graphics graphics, bool is_alternate_row)
        {
            IconListView listview = (IconListView)ParentListView;
            
            Bitmap obj = (Bitmap)this.TheObject;
            
            //draw icon
            if (obj == null)
                graphics.FillRectangle(listview.Internals.Brushes["noicon"], ScrolledBounds);
            else
            {
                Rectangle dst = IconBounds_Local;
                dst.Offset(ScrolledBounds.Left, ScrolledBounds.Top);

                graphics.DrawImage(obj, dst, 0, 0, obj.Width, obj.Height, GraphicsUnit.Pixel);
            }
        }

        public override void CalculateBounds(NeatListItem previous)
        {
            base.CalculateBounds(previous);

            IconBounds_Local = new Rectangle((ParentListView.Internals.ColumnWidth - 32) / 2, (ParentListView.Internals.RowHeight - 32) / 2, 32, 32);
        }
    }
}
