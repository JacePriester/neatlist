﻿using System.Drawing;
using NeatList;

namespace NeatList
{
    public class IconListView : NeatListView
    {
        public override void BeforePaint(Graphics graphics)
        {
            base.BeforePaint(graphics);

            Internals.Brushes["noicon"] = new SolidBrush(Properties.NoIconColor);
        }
    }
}
