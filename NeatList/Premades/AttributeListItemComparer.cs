﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeatList
{
    public class AttributeListItemComparer : IComparer<NeatListItem>
    {
        bool bReverse;
        int columnid = -1;
        bool natural_compare = true;

        public AttributeListItemComparer(bool bReverse, bool natural_compare, int column_id = -1)
        {
            this.bReverse = bReverse;
            this.natural_compare = natural_compare;
            this.columnid = column_id;
        }

        public int Compare(NeatListItem a, NeatListItem b)
        {
            AttributeListItem itema = (AttributeListItem)a;
            AttributeListItem itemb = (AttributeListItem)b;

            NeatListView listview = itema.ParentListView;
            NeatListColumn col = null;
            if (columnid == -1)
                col = listview.MultiColumnProperties.Columns[0];
            else
                col = listview.ColumnByID(columnid);

            string str_a = itema.GetAttributeString(col.AttributeName, col.AttributeIsProperty);
            string str_b = itemb.GetAttributeString(col.AttributeName, col.AttributeIsProperty);

            int val = 0;
            if (natural_compare)
                val = NaturalComparer.SingleCompare(str_a, str_b);
            else
                val = string.Compare(str_a, str_b);

            if (bReverse)
                return -1 * val;
            return val;
        }
    }
}
