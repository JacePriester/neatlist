﻿using NeatList;
using System.Collections.Generic;
using System.Drawing;

namespace NeatList
{
    public class AttributeListView : NeatListView
    {
        public class _internal_ExtraProperties
        {
            public Size GridIconSize = new Size(32, 32);
            public double GridIconZoom = 1.0;
            
            public int DetailsHMargin = 5;
            public int DetailsVMargin = 3;

            public int DetailsIconHMargin = 5;
            public int DetailsIconVMargin = 1;

            public int Tree_SpaceBetweenIconAndLabel = 5;

            public int Tree_RowMargin = 6;
            public int MultiColumn_RowMargin = 6;

            public bool Tree_UseFixedIconSize = true;
            public int Tree_IconSize = 16;
        }

        public _internal_ExtraProperties ExtraProperties = new _internal_ExtraProperties();

        public override void CalculateLayoutMeasurements()
        {
            base.CalculateLayoutMeasurements();

            if (Properties.ListFormat == NeatListFormat.MultipleColumns)
            {
                Internals.RowHeight = Internals.TextLineHeight + ExtraProperties.DetailsVMargin * 2;
            }
            else if (Properties.ListFormat == NeatListFormat.Simple)
            {
                Internals.RowHeight = (int)(ExtraProperties.GridIconSize.Height * ExtraProperties.GridIconZoom + Internals.TextLineHeight);
                Internals.ColumnWidth = (int)(Internals.RowHeight * 1.3333);
            }
            else if (Properties.ListFormat == NeatListFormat.Tree)
            {
                Internals.RowHeight = Internals.TextLineHeight + ExtraProperties.DetailsVMargin * 2;
            }
        }
    }
}
