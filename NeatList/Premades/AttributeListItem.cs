﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using NeatList;

namespace NeatList
{
    public class AttributeListItem : NeatListItem
    {
        public Rectangle NameBounds_Local; //public for rename functionality

        Dictionary<string, Rectangle> AttributeBounds_Local = new Dictionary<string, Rectangle>();

        public AttributeListItem()
        {
        }

        public AttributeListItem(object obj)
        {
            this.TheObject = obj;
        }

        public string GetAttributeString(string attribute_name, bool is_property)
        {
            if (TheObject == null)
                return null;
            
            if(is_property)
                return (string)TheObject.GetType().GetProperty(attribute_name).GetValue(TheObject);
            return (string)TheObject.GetType().GetField(attribute_name).GetValue(TheObject);
        }

        public Bitmap GetAttributeBitmap(string attribute_name, bool is_property)
        {
            if (TheObject == null)
                return null;

            if (is_property)
                return (Bitmap)TheObject.GetType().GetProperty(attribute_name).GetValue(TheObject);
            return (Bitmap)TheObject.GetType().GetField(attribute_name).GetValue(TheObject);
        }
        
        public override void CalculateBounds(NeatListItem previous)
        {
            base.CalculateBounds(previous);

            AttributeBounds_Local.Clear();

            AttributeListView listview = (AttributeListView)ParentListView;
            
            if (ParentListView.Properties.ListFormat == NeatListFormat.MultipleColumns)
            {
                Bounds.Height = ParentListView.Internals.TextLineHeight + listview.ExtraProperties.MultiColumn_RowMargin * 2;

                //currently only the first column can be the icon.
                NeatListColumn IconColumn = null;
                if (ParentListView.MultiColumnProperties.Columns.Count >= 1 && ParentListView.MultiColumnProperties.Columns[0].IsIconColumn)
                    IconColumn = ParentListView.MultiColumnProperties.Columns[0];

                int top = (Bounds.Height - listview.Internals.TextLineHeight) / 2;
                int height = listview.Internals.TextLineHeight;
                int hm = listview.ExtraProperties.DetailsHMargin;
                int vm = listview.ExtraProperties.DetailsVMargin;

                int iconsize = Math.Min(IconColumn.Bounds.Width - listview.ExtraProperties.DetailsIconHMargin * 2, IconColumn.Bounds.Height - listview.ExtraProperties.DetailsIconVMargin * 2);

                if (IconColumn != null)
                {
                    AttributeBounds_Local[IconColumn.AttributeName] = new Rectangle(IconColumn.Bounds.Left + (IconColumn.Bounds.Width - iconsize) / 2,
                    IconColumn.Bounds.Top + (IconColumn.Bounds.Height - iconsize) / 2,
                    iconsize,
                    iconsize);
                }

                foreach (NeatListColumn col in ParentListView.MultiColumnProperties.Columns)
                {
                    if (col == IconColumn)
                        continue;

                    AttributeBounds_Local[col.AttributeName] = new Rectangle(col.Bounds.Left + hm, top, col.Bounds.Width - hm * 2, height);
                }
            }
            else if (ParentListView.Properties.ListFormat == NeatListFormat.Simple)
            {
                int iconw = (int)(listview.ExtraProperties.GridIconSize.Width * listview.ExtraProperties.GridIconZoom);
                int iconh = (int)(listview.ExtraProperties.GridIconSize.Height * listview.ExtraProperties.GridIconZoom);

                AttributeBounds_Local.Clear();

                if (ParentListView.Properties.ItemLayout == NeatItemLayout.Rows)
                {

                    //currently only the first column can be the icon.
                    if (ParentListView.MultiColumnProperties.Columns.Count >= 1 && ParentListView.MultiColumnProperties.Columns[0].IsIconColumn)
                    {
                        NeatListColumn IconColumn = ParentListView.MultiColumnProperties.Columns[0];
                        AttributeBounds_Local[IconColumn.AttributeName] = new Rectangle((Bounds.Width - iconw) / 2, (Bounds.Height - iconh) / 2, iconw, iconh);
                    }
                    else
                    {
                        int top = (Bounds.Height - listview.Internals.TextLineHeight) / 2;
                        int height = listview.Internals.TextLineHeight;
                        int hm = listview.ExtraProperties.DetailsHMargin;
                        int vm = listview.ExtraProperties.DetailsVMargin;

                        //common string format for single-line stuff
                        //var fmt = new StringFormat();
                        //fmt.Alignment = StringAlignment.Near;
                        //fmt.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.NoClip | StringFormatFlags.LineLimit;
                        //fmt.LineAlignment = StringAlignment.Near;
                        //fmt.Trimming = StringTrimming.None;

                        //using (Graphics graphics = listview.CreateGraphics())
                        //{
                        NeatListColumn TextColumn = ParentListView.MultiColumnProperties.Columns[0];
                        AttributeBounds_Local[TextColumn.AttributeName] = new Rectangle(hm, top, Bounds.Width - hm * 2, height);

                        //SizeF sz = graphics.MeasureString(GetAttributeString(TextColumn.AttributeName, TextColumn.AttributeIsProperty), listview.Font, Bounds.Width, fmt);
                        Size sz = TextRenderer.MeasureText(GetAttributeString(TextColumn.AttributeName, TextColumn.AttributeIsProperty), listview.Font);
                        NameBounds_Local = new Rectangle((Bounds.Width - sz.Width) / 2, Bounds.Height - listview.Internals.TextLineHeight, sz.Width, listview.Internals.TextLineHeight);
                        //}
                    }
                }
                else if (ParentListView.Properties.ItemLayout == NeatItemLayout.Columns)
                {
                    if (ParentListView.MultiColumnProperties.Columns.Count != 1)
                    {
                        throw new Exception("Only 1 column is supported for AttributeListItem in ItemLayout=Columns mode right now and it has to be a string.");
                    }
                    else
                    {
                        int top = (Bounds.Height - listview.Internals.TextLineHeight) / 2;
                        int height = listview.Internals.TextLineHeight;
                        int hm = listview.ExtraProperties.DetailsHMargin;
                        int vm = listview.ExtraProperties.DetailsVMargin;

                        NeatListColumn TextColumn = ParentListView.MultiColumnProperties.Columns[0];
                        Size sz = TextRenderer.MeasureText(GetAttributeString(TextColumn.AttributeName, TextColumn.AttributeIsProperty), listview.Font);

                        int l;
                        if (previous != null)
                            l = previous.Bounds.Right + hm;
                        else
                            l = ParentListView.Properties.LeftMargin;

                        Bounds = new Rectangle(l, ParentListView.Properties.TopMargin, sz.Width + hm*2, ParentListView.Internals.TextLineHeight + vm * 2);

                        AttributeBounds_Local[TextColumn.AttributeName] = new Rectangle(hm, vm, sz.Width, sz.Height);
                        NameBounds_Local = AttributeBounds_Local[TextColumn.AttributeName];
                    }
                }
            }
            else if (ParentListView.Properties.ListFormat == NeatListFormat.Tree)
            {
                Bounds.Height = ParentListView.Internals.TextLineHeight + listview.ExtraProperties.Tree_RowMargin * 2;

                //common string format for single-line stuff
                var fmt = new StringFormat();
                fmt.Alignment = StringAlignment.Near;
                fmt.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.NoClip | StringFormatFlags.LineLimit;
                fmt.LineAlignment = StringAlignment.Near;
                fmt.Trimming = StringTrimming.None;

                AttributeBounds_Local.Clear();

                NeatListColumn IconColumn = null;

                if (ParentListView.MultiColumnProperties.Columns.Count >= 1 && ParentListView.MultiColumnProperties.Columns[0].IsIconColumn)
                {
                    IconColumn = ParentListView.MultiColumnProperties.Columns[0];

                    int iconsize;
                    if (listview.ExtraProperties.Tree_UseFixedIconSize)
                        iconsize = listview.ExtraProperties.Tree_IconSize;
                    else
                        iconsize = Bounds.Height - listview.ExtraProperties.DetailsIconVMargin * 2;

                    AttributeBounds_Local[IconColumn.AttributeName] = new Rectangle(ParentListView.TreeProperties.ObjectInteriorLeftMargin,
                        (Bounds.Height - iconsize) / 2,
                        iconsize,
                        iconsize);
                }

                NeatListColumn TextColumn = null;

                if (IconColumn == null && ParentListView.MultiColumnProperties.Columns.Count >= 1 && ParentListView.MultiColumnProperties.Columns[0].IsIconColumn == false)
                {
                    TextColumn = ParentListView.MultiColumnProperties.Columns[0];
                }
                else if (ParentListView.MultiColumnProperties.Columns.Count >= 2)
                {
                    TextColumn = ParentListView.MultiColumnProperties.Columns[1];
                }

                if(TextColumn != null)
                {
                    if (IconColumn != null)
                    {
                        AttributeBounds_Local[TextColumn.AttributeName] = new Rectangle(AttributeBounds_Local[IconColumn.AttributeName].Right + listview.ExtraProperties.Tree_SpaceBetweenIconAndLabel,
                        (Bounds.Height - listview.Internals.TextLineHeight) / 2,
                        listview.Internals.ContentRect.Width, //doesn't matter
                        listview.Internals.TextLineHeight);
                    }
                    else
                    {
                        AttributeBounds_Local[TextColumn.AttributeName] = new Rectangle(ParentListView.TreeProperties.ObjectInteriorLeftMargin,
                        (Bounds.Height - listview.Internals.TextLineHeight) / 2,
                        listview.Internals.ContentRect.Width, //doesn't matter
                        listview.Internals.TextLineHeight);
                    }
                }

            }
        }

        public override void Draw(Graphics graphics, bool is_alternate_row)
        {
            AttributeListView listview = (AttributeListView)ParentListView;

            if (ParentListView.Properties.ListFormat == NeatListFormat.MultipleColumns || ParentListView.Properties.ListFormat == NeatListFormat.Simple || ParentListView.Properties.ListFormat == NeatListFormat.Tree)
            {
                //common string format for single-line stuff
                var fmt = new StringFormat();
                fmt.Alignment = StringAlignment.Near;
                fmt.FormatFlags = StringFormatFlags.NoWrap;
                fmt.LineAlignment = StringAlignment.Near;
                fmt.Trimming = StringTrimming.None;

                foreach (NeatListColumn col in ParentListView.MultiColumnProperties.Columns)
                {
                    if (col.IsIconColumn)
                    {
                        //draw icon
                        Rectangle icon_bounds = AttributeBounds_Local[col.AttributeName];
                        icon_bounds.Offset(ScrolledBounds.Location);

                        Bitmap image = GetAttributeBitmap(col.AttributeName, col.AttributeIsProperty);
                        if(image != null)
                            graphics.DrawImage(image, icon_bounds, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                        else
                            graphics.FillRectangle(ParentListView.Internals.Brushes["noicon"], icon_bounds);
                    }
                    else
                    {
                        //RectangleF recf = new RectangleF(AttributeBounds_Local[col.AttributeName].Left, AttributeBounds_Local[col.AttributeName].Top, AttributeBounds_Local[col.AttributeName].Width, AttributeBounds_Local[col.AttributeName].Height);
                        //recf.Offset(ScrolledBounds.Location);

                        Point textloc = new Point(AttributeBounds_Local[col.AttributeName].Left, AttributeBounds_Local[col.AttributeName].Top);
                        textloc.Offset(ScrolledBounds.Location);

                        TextRenderer.DrawText(graphics, GetAttributeString(col.AttributeName, col.AttributeIsProperty), ParentListView.Font, textloc, ParentListView.ForeColor);
                        //graphics.DrawString(GetAttributeString(col.AttributeName, col.AttributeIsProperty), ParentListView.Font, ParentListView.Internals.Brushes["text"], recf, fmt);
                    }
                }
            }
        }

        public override void EvaluateCanExpand()
        {
            CanExpand = true; //always!
        }
    }
}
