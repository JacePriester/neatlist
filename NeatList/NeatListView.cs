﻿using System.Windows.Forms;
using System.Collections.Generic;
using System;
using WindowsTheme;
using System.Drawing;
using System.Windows.Forms.VisualStyles;

namespace NeatList
{
    public abstract partial class NeatListView : Control
    {
        public _internal_Properties Properties = new _internal_Properties();
        public _internal_Internals Internals = new _internal_Internals();
        public _internal_MultiColumnProperties MultiColumnProperties = new _internal_MultiColumnProperties();
        public _internal_GridLayoutProperties GridLayoutProperties = new _internal_GridLayoutProperties();
        public _internal_RowColLayoutProperties RowColLayoutProperties = new _internal_RowColLayoutProperties();
        public _internal_TreeProperties TreeProperties = new _internal_TreeProperties();

        public delegate void NeatListViewItemEventHandler(NeatListView list, NeatListItem item);
        public delegate void NeatListColumnEventHandler(NeatListView list, NeatListColumn column);
        public delegate void NeatListViewColorsChangedEventhandler(NeatListView list);

        public event NeatListColumnEventHandler ColumnHeaderClick;
        public event NeatListColumnEventHandler ColumnHeaderRightClick;
        public event NeatListViewItemEventHandler ItemActivate;
        public event NeatListViewItemEventHandler ItemActivateMiddleButton;
        public event NeatListViewItemEventHandler ItemRightClick;
        public event NeatListViewItemEventHandler NonItemRightClick;
        public event NeatListViewItemEventHandler SelectionChanged; //can send for a null item; receiver should check SelectedItems instead for multi-select lists
        public event NeatListViewItemEventHandler HighlightedItemChanged; //can send for a null item
        public event NeatListViewItemEventHandler ItemExpanded;
        public event NeatListViewItemEventHandler ItemCollapsed;
        public event NeatListViewColorsChangedEventhandler ColorsChanged;

        public ToolTip ToolTip = new ToolTip();
        private Timer TooltipTimer = new Timer();

        private _internal_CalculateBoundsResults CalculateBoundsResults = new _internal_CalculateBoundsResults();
        private _internal_PaintInfo PaintInfo = new _internal_PaintInfo();

        private VScrollBar VerticalScroll = new VScrollBar();
        private HScrollBar HorizontalScroll = new HScrollBar();

        public void BeginModifyList()
        {
            Internals.OutsideCodeModifyingList = true;
        }

        public void EndModifyList()
        {
            Internals.OutsideCodeModifyingList = false;
            Modified();
        }

        public void ActivateHighlightedItem()
        {
            if (Internals.HighlightedItem == null)
                return;
            ItemActivate?.Invoke(this, Internals.HighlightedItem);
        }

        public void ActivateSelectedItem()
        {
            if (Internals.SelectedItem == null)
                return;
            ItemActivate?.Invoke(this, Internals.SelectedItem);
        }



        public int GetItemIndex(NeatListItem item)
        {
            lock (Internals.ThreadSafety)
            {
                return Internals.TopLevelItems.IndexOf(item);
            }
        }

        public NeatListItem GetItemAtIndex(int index)
        {
            lock (Internals.ThreadSafety)
            {
                if (index < 0 || index >= Internals.TopLevelItems.Count)
                    return null;
                return Internals.TopLevelItems[index];
            }
        }

        public void HighlightPrev()
        {
            _HighlightOffset(-1);
        }

        public void HighlightNext()
        {
            _HighlightOffset(1);
        }

        public void SelectPrev()
        {
            _SelectOffset(-1);
        }

        public void SelectNext()
        {
            _SelectOffset(1);
        }

        public NeatListHitTestResults HitTest(int x, int y)
        {
            NeatListHitTestResults hitres = new NeatListHitTestResults();
            lock (Internals.ThreadSafety)
            {
                if (Properties.ListFormat == NeatListFormat.MultipleColumns && MultiColumnProperties.ColumnHeadersClickable)
                {
                    HitTestColumnHeaders(ref hitres, x, y);
                    if (hitres.RegisteredHit)
                        return hitres;
                }
                
                HitTestForChildren(null, ref hitres, x, y);
            }

            return hitres;
        }

        void HitTestColumnHeaders(ref NeatListHitTestResults hitres, int x, int y)
        {
            foreach (NeatListColumn column in MultiColumnProperties.Columns)
            {
                if (column.Bounds.Contains(x, y))
                {
                    hitres.RegisteredHit = true;
                    hitres.HitColumnHeader = true;
                    hitres.HitColumn = column;
                    return;
                }
            }
        }
        
        public void ClearItems()
        {
            lock (Internals.ThreadSafety)
            {
                Internals.TopLevelItems.Clear();
                Internals.HighlightedItem = null;
                Internals.SelectedItem = null;
                Internals.SelectedItems.Clear();
                Modified();
            }
        }

        public void Modified()
        {
            Modified(false);
        }
        
        public void SetHighlightedItem(NeatListItem new_highlighted_item)
        {
            lock (Internals.ThreadSafety)
            {
                if (Internals.HighlightedItem != new_highlighted_item)
                {
                    Internals.HighlightedItem = new_highlighted_item;
                    SmartInvalidate();

                    if (Properties.SupportsTooltips)
                        StartTooltipTimeout();

                    HighlightedItemChanged?.Invoke(this, new_highlighted_item);
                }
            }
        }

        public void SetSelectedItem(NeatListItem new_selected_item)
        {
            lock (Internals.ThreadSafety)
            {
                if (Internals.SelectedItem != new_selected_item)
                {
                    Internals.SelectedItem = new_selected_item;
                    Internals.SelectedItems.Clear();
                    Internals.SelectedItems.Add(new_selected_item);
                    SmartInvalidate();

                    SelectionChanged?.Invoke(this, new_selected_item);
                }
            }
        }

        public void SetSelectedItems(List<NeatListItem> new_items)
        {
            lock (Internals.ThreadSafety)
            {
                NeatListItem prev_SelectedItem = Internals.SelectedItem;
                List<NeatListItem> prev_SelectedItems = new List<NeatListItem>();
                foreach (NeatListItem item in Internals.SelectedItems)
                    prev_SelectedItems.Add(item);

                if (new_items == null || new_items.Count == 0)
                {
                    Internals.SelectedItem = null;
                    Internals.SelectedItems.Clear();
                }
                else
                {
                    Internals.SelectedItem = new_items[0];

                    Internals.SelectedItems.Clear();
                    foreach (NeatListItem item in new_items)
                    {
                        Internals.SelectedItems.Add(item);
                    }
                }

                NotifyIfSelectionChanged(prev_SelectedItem, prev_SelectedItems);
            }
        }
        
        public void ResetScroll()
        {
            SetScroll(0, 0);
        }

        public void SetScroll(int x, int y)
        {
            Internals.ScrollX = x;
            Internals.ScrollY = y;
            CalculateScrolledBounds();
            SmartInvalidate();
        }
        
        public void PassMouseWheel(MouseEventArgs e)
        {
            OnMouseWheel(e);
        }

        public void SetItemLayout(NeatItemLayout ItemLayout)
        {
            Properties.ItemLayout = ItemLayout;
            Modified();
        }

        public void SetListFormat(NeatListFormat ListFormat)
        {
            Properties.ListFormat = ListFormat;
            Modified();
        }

        public NeatListColumn ColumnByID(int ID)
        {
            lock (Internals.ThreadSafety)
            {
                foreach (NeatListColumn col in MultiColumnProperties.Columns)
                {
                    if (col.ID == ID)
                        return col;
                }
            }
            return null;
        }

        public void ToggleExpanded(NeatListItem item)
        {
            if (!item.IsExpanded)
                Expand(item);
            else
                Collapse(item);
        }

        public void Expand(NeatListItem item)
        {
            if (!item.IsExpanded)
            {
                bool proceed = item.BeforeExpand();
                if (item.CanExpand && proceed)
                {
                    item.IsExpanded = true;
                    item.OnExpand();
                    ItemExpanded?.Invoke(this, item);
                    Modified();
                }
            }
        }

        public void Collapse(NeatListItem item)
        {
            if (item.IsExpanded)
            {
                bool proceed = item.BeforeCollapse();
                if (item.CanCollapse && proceed)
                {
                    item.IsExpanded = false;
                    item.OnCollapse();
                    ItemCollapsed?.Invoke(this, item);
                    Modified();
                }
            }
        }

        public NeatListItem LookupItem(object TheObject)
        {
            return LookupItem(null, TheObject);
        }

        public NeatListItem LookupItem(NeatListItem ContainerItem, object TheObject)
        {
            lock (Internals.ThreadSafety)
            {
                List<NeatListItem> items = ContainerItem == null ? Internals.TopLevelItems : ContainerItem.ChildItems;

                foreach (NeatListItem item in items)
                {
                    if (item.TheObject == TheObject)
                        return item;

                    //look through tree, depth-first.
                    LookupItem(item, TheObject);
                }
            }

            return null;
        }

        public void SetItems(List<NeatListItem> Items)
        {
            SetItems(null, Items);
        }

        //swap items or subitems. try to persist selection if some new items are the same.
        public void SetItems(NeatListItem ContainerItem, List<NeatListItem> Items)
        {
            lock (Internals.ThreadSafety)
            {
                List<NeatListItem> olditems = null;
                if (ContainerItem == null)
                {
                    olditems = Internals.TopLevelItems;
                    Internals.TopLevelItems = Items;
                }
                else
                {
                    olditems = ContainerItem.ChildItems;
                    ContainerItem.ChildItems = Items;
                }

                if (!Properties.PreserveSelectionByObject)
                {
                    foreach (NeatListItem olditem in olditems)
                    {
                        if (!Items.Contains(olditem)) //the item has really been removed, so make sure it's not in selection.
                        {
                            if (Internals.SelectedItems.Contains(olditem))
                                Internals.SelectedItems.Remove(olditem);
                            if (Internals.SelectedItem == olditem)
                                Internals.SelectedItem = null;
                        }
                    }
                }
                else //preserve selection by object instead of by item
                {
                    List<object> oldobjects = new List<object>();
                    foreach (NeatListItem item in olditems)
                        oldobjects.Add(item.TheObject);

                    List<object> newobjects = new List<object>();
                    foreach (NeatListItem item in Items)
                        newobjects.Add(item.TheObject);

                    List<NeatListItem> sel_remove = new List<NeatListItem>();

                    foreach (object oldobject in oldobjects)
                    {
                        if (!newobjects.Contains(oldobject)) //object really has been removed, make sure its not in selection
                        {
                            foreach (NeatListItem selitem in Internals.SelectedItems)
                            {
                                if (selitem.TheObject == oldobject)
                                    sel_remove.Add(selitem);
                            }
                            if (Internals.SelectedItem != null && Internals.SelectedItem.TheObject == oldobject)
                            {
                                Internals.SelectedItem = null;
                            }
                        }
                    }

                    foreach (NeatListItem item in sel_remove)
                        Internals.SelectedItems.Remove(item);

                    //now, relink selection based on the object->item mapping
                    Dictionary<object, NeatListItem> obj_to_newitem = new Dictionary<object, NeatListItem>();
                    foreach (NeatListItem newitem in Items)
                        obj_to_newitem[newitem.TheObject] = newitem;

                    if (Internals.SelectedItem != null && obj_to_newitem.ContainsKey(Internals.SelectedItem.TheObject))
                        Internals.SelectedItem = obj_to_newitem[Internals.SelectedItem.TheObject];

                    List<NeatListItem> newselection = new List<NeatListItem>();
                    foreach (NeatListItem selitem in Internals.SelectedItems)
                    {
                        if (obj_to_newitem.ContainsKey(selitem.TheObject))
                            newselection.Add(obj_to_newitem[selitem.TheObject]);
                        else
                            newselection.Add(selitem);
                    }
                    Internals.SelectedItems.Clear();
                    foreach (NeatListItem newselitem in newselection)
                        Internals.SelectedItems.Add(newselitem);
                }

                Modified();
            }
        }

        public void Modified(bool force_refresh)
        {
            if (Internals.OutsideCodeModifyingList && !force_refresh)
                return;

            CalculateBounds();
        }

        public void Sort(NeatListItem ContainerItem, IComparer<NeatListItem> Comparer, bool sort_recursive)
        {
            lock (Internals.ThreadSafety)
            {
                Properties.SortComparer = Comparer;

                if (Comparer == null)
                    return;

                List<NeatListItem> items = ContainerItem == null ? Internals.TopLevelItems : ContainerItem.ChildItems;

                bool cansort = (ContainerItem == null) ? Properties.AllowSortTopLevelItems : ContainerItem.AllowSortChildren;

                if (cansort)
                {
                    items.Sort(Comparer);

                    if (sort_recursive)
                    {
                        foreach (NeatListItem item in items)
                            Sort(item, Comparer, sort_recursive);
                    }
                }

                Modified();
            }
        }

        public void Sort(IComparer<NeatListItem> Comparer, bool sort_children)
        {
            Sort(null, Comparer, sort_children); //fixme: i changed this from true to sort_children, was that right??
        }

        public void Sort(IComparer<NeatListItem> Comparer)
        {
            Sort(Comparer, Properties.ListFormat == NeatListFormat.Tree);
        }

        public void ReSort()
        {
            ReSort(null);
        }

        public void ReSort(NeatListItem ContainerItem)
        {
            if (Properties.SortComparer == null && Properties.ThrowIfReSortWhenSortComparerIsNull)
                throw new Exception("ReSort() called for list while Properties.SortComparer was null. You can disable this exception if you want.");

            Sort(ContainerItem, Properties.SortComparer, Properties.ListFormat == NeatListFormat.Tree);
        }

        public static List<NeatListItem> ObjectsToItems<NeatListItemDerivedType, ObjectType>(List<ObjectType> objects) where NeatListItemDerivedType: NeatListItem, new()
        {
            List<NeatListItem> items = new List<NeatListItem>();
            foreach (object o in objects)
            {
                NeatListItemDerivedType item = new NeatListItemDerivedType();
                item.TheObject = o;
                items.Add(item);
            }
            return items;
        }

        public void ScrollToItem(NeatListItem item)
        {
            if (item == null)
                return;

            int column_header_height = 0;
            if (Properties.ListFormat == NeatListFormat.MultipleColumns)
                column_header_height = Internals.ColumnHeaderHeight;
            SetScroll(Properties.EnableHorizontalScroll ? item.Bounds.Left : 0, -column_header_height + (Properties.EnableVerticalScroll ? item.Bounds.Top : 0));
        }

        public void ScrollToSelected()
        {
            ScrollToItem(Internals.SelectedItem);
        }

        public void ColorsFromTheme()
        {
            ThemeColorData acd = ThemeInfo.GetThemeColor();
            
            Properties.HighlightColor = acd.Lighter;
            Properties.SelectionColor = ThemeInfo.BlendColor(Color.White, 0.3, SystemColors.Highlight);
            Properties.AlternateRowBackColor = ThemeInfo.BlendColor(BackColor, 0.075, acd.Lighter);
            Properties.EmptyListBackColor = new System.Windows.Forms.ToolTip().BackColor;

            MultiColumnProperties.ColumnHeaderBackgroundColor = SystemColors.Control;
            MultiColumnProperties.ColumnHeaderBorderColor = SystemColors.ControlDark;

            //thats probably sufficient; the rest dont really conform to theme items.
            //fixme: maybe the window background color and text color?

            //give the creating window the opportunity to set any colors back
            ColorsChanged?.Invoke(this);
        }
    }
}